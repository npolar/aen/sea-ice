#!/bin/bash
# A helper script to run Opendronemap (https://opendronemap.org) on images from
# the ANAFI USA RPAS - to generate orthophotos and a representation of terrain
# for sea ice sampling sites

docker run -ti --rm -v $(pwd):/datasets/code --gpus all opendronemap/odm:gpu \
 --project-path /datasets \
 --camera-lens brown \
 --pc-quality high \
 --feature-quality high \
 --min-num-features 60000 \
 --gps-accuracy 1.0 \
 --use-3dmesh \
 --dem-resolution 5 \
 --orthophoto-resolution 2 \
 --build-overviews \
 --cog \
 --orthophoto-png \
 --dsm \
 --dtm \
 --pc-las \
 --pc-filter 1.5 \
 --pc-sample 0.01 \
 --rerun-all \
 --optimize-disk-space \
 --max-concurrency 18 \
 --time \
 -v
