# Flight planning for drifting sea ice

**Adam Steer**, September 2022

Mapping flights on sea ice are unusual because the site is always drifting - with ANAFI USA at the time of writing, this means fully manual flight.

This document outlines principles of flight, applied in Nansen Legacy research work.

## General principles

ANAFI-USA is used for mapping ice floes using *Structure-from-motion* (SfM) photogrammetric techniques [link to Snavely et al 2006]. We want to reconstruct the topography and structure of an ice floe - or whatever else we are mapping. To make that work, we need a lot of highly overlapping imagery. Our aim is to fly lines which allow image capture of at least 60% overlap *along track* and 60% *across track*. Because sea ice is constantly drifting we need to fly these lines manually, without semi-autonomous flight planning help.

#### Look after yourself

Flying for mapping sea ice is cold, and demands a lot of attention. You have to keep track of a dot in the sky for up to 30 minutes at a single battery, then do it again twice more if all goes well and you're able to consume all available batteries. **If your attention starts to wander, end the mission**! Take a break, warm up, restore yourself.

#### Preflight training

A critical piloting skill on sea ice is visual assessment of where the aircraft is in space. A great way to do this is to set up practice drills:

- set out ground targets at different distances (20, 50, 100m)
- have the pilot fly at 30-40m altitude and hover over the target without using the camera as an aid
- attempt to land on the target by straight down descent

This skill gives the pilot a great sense of where the aircraft is in 3 dimensional space.

A second critical skill is aircraft location, orientation and retrieval. Have one pilot fly the aircraft away while a second pilot is not looking, then hand over controls and have the second pilot locate and return the aircraft - ideally without using on-screen aids.

Become very familiar with the layout of the Freeflight 6 controlling app. Understand how to read distances, speeds, battery levels, and drone orientation without losing track of the aircraft in the air. Understand how to use the controlling app to recover the aircraft if you do lose visual track (formally, this is an illegal operating state - however, it can happen on sea ice for several reasons)

Finally, practice all of the flight guidance given below with the aircraft before heading to sea.

## Flight guidance

OpenDroneMap's tutorials offer excellent guidance on flight patterns for various tasks: https://docs.opendronemap.org/tutorials/

On sea ice, the piloting challenge is to replicate those patterns using manual flight. We also need to consider our environment. At high latitude, the aircraft navigation computer has a lot of conflicting instructions to handle. How we direct the aircraft in the air needs to be modified to account for a far more difficult navigation environment.

#### Understand image coverage

It helps a lot to understand the relationship between flying height and image coverage. A guidance table for ANAFI USA is given below. Combined with practice at aircraft spatial awareness, this helps a lot in creating quality mapping data from manual flights.

| flight height | along-track | across-track | pixel size |
|---------------|-------------|--------------|------------|
| 10 | 10 | 11 | 0.0028 |
| 20 | 22 | 30 | 0.0055 |
| 30 | 33 | 44 | 0.0083 |
| 40 | 44 | 59 | 0.011 |
| 50 | 55 | 74 | 0.0138 |
| 60 | 67 | 89 | 0.0166 |
| 70 | 79 | 103 | 0.0193 |
| 80 | 89 | 118 | 0.0221 |

From this table, you can infer that flying at 40 m above ground, you need to estimate flight lines approximately 25 m apart to get good overlap between lines. Using GPS lapse to collect an image every 5 m ensures great along-track image overlap.

#### Use GPS lapse mode

ANAFI USA helps us a lot with a GPS lapse photography mode. This setting automatically captures an image every time the aircraft moves a given distance in 3D space. Set it to `5 meters`.

#### Consider wind

Wind at 40 m height will often be very different from wind at sea level. Consider speed and direction recorded at the ship, **do not launch above 12 meters per second recorded at the ship**.

ANAFI USA's published wind resistance is 15 meters per second. This does not mean it can fly a survey pattern in 15 meter per second wind, because you need power in reserve to fly against the wind. Above a few meters per second wind, always fly in `racing mode`, allowing the aircraft to use its full power. This means practicing in racing mode also, and some effort to monitor airspeed.

#### Flight speed

Aim to keep the aircraft airspeed below 5 meters per second. This helps reduce motion effects and distortion due to camera motion while iamges are being captured.

Airspeed is displayed in the top left of the Freeflight 6 display.

#### Avoid hover states

At very high latitudes, hovering is the time when the aircraft navigation system is most vulnerable to confusion. Keeping the aircraft in motion helps avoid loss of control authority. Use wide turns, keep hover states (eg to frame or capture an image) at a minimum.

Practice sideways flight and turning at the same time to keep an object in the camera view without hovering.

#### Use a limited-direction flight pattern

If it is difficult to see aircraft orientation, use pattern in which the aircraft faces one way and all motion is limited to forward/backward/left/right. See the diagrams below for a visual guide. The first shows an approximate flight strategy, including gimbal angles near the ship:

[<img src="images/site-flight-overview.png" width="600"/>](images/site-flight-overview.png)

*Figure 1: Flight plan overview for limited direction flights*

The next is an overhead view aiming to show how aicraft orientation looks for each flown 'grid':

[<img src="images/squarepattern.png" width="600"/>](images/squarepattern.png)

*Figure 2: Overhead view of a limited direction flight plan*

#### Use multiple flight heights

Cover an area at the lowest altitude first - this is the most demanding flying. Then increase height by up to 10 m and repeat the coverage.

#### Fill details with circular flights

For regions containing complex objects of high interest, using a circular flight pattern with the camera pointed toward the object helps to fill in details. In this mode, set the gimbal to a fairly low angle, and lower flight altitude. This helps capture detail not observable from high altitude, downward looking cameras.

[<img src="images/circle-flight.png" width="600"/>](images/circle-flight.png)

*Figure 3: Sketch showing the general principles of a circular flight plan focussed on an object of interest*

#### Gimbal angles

For mapping, set the Gimbal to slightly off nadir, `80 to 85 degrees`. For a flightline along the ship with the camera aimed at the ship, tilt the camera up slightly to around `60 degrees`, then repeat the same line with the camera at normal inclination.

Off nadir imagery generally helps address distortion when building 3D models / orthophotos.

#### Include the ship for context

Respecting distances to the ship, try to capture the side and partial deck of the ship parked at the ice. It provides context, and can be used as a quick scale check using know ship dimensions.

## Pilot maintenance

Flying for mapping in polar regions - especially in winter - is cold, stationary, and boring. Use these tips as guidance for staying focussed and safe.

#### Prepare early

Set up all the flight settings in Freeflight 6 before you go outside. It helps a lot if you don't need to touch the screen at all outside of calibration.

#### Layer 0

In an outdoor layering system, the first layer is your fuel. Make sure you've eaten well and avoided heavy foods (carbs, not fats before a mission. Fats at the end of the day for tomorrow's work). Maintain water levels, and pee as soon as you need to! Better to take pee breaks than let your water levels down.

#### Cold core, cold hands

Your hands will be the first part to suffer. if your core is cold, you can't keep your hands warm - so overload your core. A down vest or thermal singlet is a great layering addition, even if it feels too hot at the start.

Use liner gloves for fine work (use touchscreen gloves), and have warm outer gloves or mittens for being on the controls. The Skycontroller can be operated in doubled gloves or even mittens

#### Scout your piloting zones

You can, and will, move around while piloting. Scout the area around where you plan to operate, so you can move safely while keeping your main attention on the aircraft.

#### Take breaks

There are no prizes for cold endurance!

#### Flying with vision aids

If you can, use contact lenses for days when you're flying. That simplifies things a lot! Use good quality goggles designed for maximum venting and visual acuity in flat light.

Wearing glasses under goggles is extremely tricky and not recommended - on sea ice it is very very easy to overload goggle venting systems and fog up.
