# ANAFI USA operations checklist

A detailed checklist can be found in the ANAFI USA user manual, contained in this repository. This checklist is a reminder of the bare essentials.

## Preflight

| Flight log record |
|----------------------|
| :white_large_square: Pilot name  |
| :white_large_square: date and time |
| :white_large_square: location / site ID  |
| :white_large_square: weather conditions  |

| Software updates |
|---------------------|
| :white_large_square: Skycontroller updated (date:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; )|
| :white_large_square: Freeflight 6 updated (date:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ) |
| :white_large_square: Drone updated (date:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ) |

| Camera setup |
|--------------|
| :white_large_square: Camera calibrated if needed |
| :white_large_square: Thermal and RGB boresight adjusted |

## Flight time checks

| ANAFI USA checks before power on |
|-------------------------------------|
| :white_large_square: radio to KPH bridge for start of flight operations |
| :white_large_square: Arms unfolded and locked |
| :white_large_square: Arms free of obstructions |
| :white_large_square: Lens cap off and stowed |
| :white_large_square: Drone and gimbal OK with visual and tactile check |
| :white_large_square: Propellors undamaged and freely moving |
| :white_large_square: Motors OK (not crunchy, move easily turned by hand) |
| :white_large_square: Batteries charged (test shows 4 lights) |
| :white_large_square: SD card inserted and locked |
| :white_large_square: Battery engaged with 3 hooks, and locked |
| :white_large_square: Control device set to flight mode (wifi off, bluetooth off) |

| Power on |
|-------------|
| :white_large_square: Battery charged (4 lights)|
| :white_large_square: Motor self test complete |
| :white_large_square: Gimbal self test complete |
| :white_large_square: Controller connected to drone |

| Calibration tasks |
| ------------------|
| :white_large_square: Skycontroller 3 calibrated |
| :white_large_square: ANAFI USA compass calibrated |
| :white_large_square: ANAFI USA camera calibrated as needed |

| Prelaunch checks |
|---------------|
| :white_large_square: ANAFI USA GPS green |
| :white_large_square: Skycontroller GPS green |
| :white_large_square: RTH height set to: |
| :white_large_square: Geofence confirmed (if desired) |
| :white_large_square: Flight mode set to:  |
| :white_large_square: Camera mode set to:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  |

| Launch |
|--------|
| *Ice launch:* prepare a smooth takeoff and landing site and set the aicraft down for normal launch and landing |
| *Ship launch:* do not set the aicraft down on deck - use a hand launch procedure |
| :white_large_square: take off and fly to 5 m away, 2 m off ground |
| :white_large_square: listen - are engines and propellors sounding normal? |
| :white_large_square: wind check - land immediately and check flight mode settings if drifting |
| :white_large_square: control check - ensure aircraft response to all controls |
| :white_large_square: start camera in GPS lapse mode |


| In flight |
|-----------|
| :white_large_square: ensure aircraft responds to instruction |
| *In loss of control authority situations, ensure good line of sight to aircraft, move nearer to aircraft, issue strong control instructions eg full speed, hard turns. Try to fly aircraft away from people and infrastructure, crash if needed* |
| :white_large_square: keep radio on, listen and respond to commands from KPH bridge, bear guards or piloted aircraft |
| :white_large_square: monitor battery charge and distances |

| Landing |
|---------|
| :white_large_square: return to base at 25-30% battery charge |
| :white_large_square: stop camera |
| :white_large_square: on landing, immediately turn aircraft power off |

| Relaunch and continue missions |
|--------------------------------|
| :white_large_square: inspect for damage. if physical damage has occurred, end mission |
| :white_large_square: inspect for icing, remove icing if present |
| :white_large_square: repeat the power on, calibration, prelaunch and launch lists |
| **Do not skip calibrating the aircraft on battery change and relaunch** |

| End of mission |
|----------------|
| :white_large_square: radio KPH bridge for end of drone operations |
| :white_large_square: replace lens cap, pack away |
| :white_large_square: bring aircraft inside and allow to warm with the case closed |

| Post mission - once aicraft is warm |
|-------------------------------------|
| :white_large_square: remove SD card, copy contents to a backup location |
| :white_large_square: format SD card ready for next flight |
| :white_large_square: inspect closely for damage, wear and tear |
| :white_large_square: wipe down all parts with a damp cloth (fresh water) |
| :white_large_square: charge all batteries |
| :white_large_square: charge controller and controlling device |
| :white_large_square: pack away ready for next flight |
