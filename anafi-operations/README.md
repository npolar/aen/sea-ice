# ANAFI-operations

A home for documentation for the AeN ANAFI USA drone, including operations manuals

Developed by Adam Steer.

[**Operations manual**](operations-manual.md) - A description of the aircraft system and  guidelines for operating the NPI Parrot ANAFI USA off Kronprins Haakon.

[**Checklist**](checklist.md) - A checklist for operating ANAFI USA from Kronprins Haakon, derived from the comprehensive checklist in the ANAFI USA user guidelines

[**Flight guidance**](flight-guidance.md) - tips for successful sea ice mapping with ANAFI USA

[**Risk assessment template**](risk-assessment.md) - common items for risk assessments when ANAFI USA deployment is planned, using a common approach where hazard, exposure frequency, consequences, preventative and corrective measures are all considered.

[**Data overview**](data-overview.md) - using and managing data captured during ANAFI USA operations.

[**User guide**](anafi-usa-user-guide.pdf) - Parrot's comprehensive ANAFI USA user guide, including the Freeflight 6 interface and Skycontroller 3 operation. It is highly recommended reading.
