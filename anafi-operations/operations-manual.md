# ANAFI USA operations manual - Norwegian Polar Institute

## Introducing the ANAFI USA

The ANAFI USA is a small remotely piloted aircraft system (RPAS) (500gm) manufactured by Parrot. Its key features are portability, 15 m/s wind resistance, and IP53 weather resistance. It carries a dual 21 MP Sony sensor, one dedicated to wide angle imagery and one dedicated to zoomed in imagery. It also carries a FLIR BOSON thermal infrared sensor.

It uses a combined multi-constellation GNSS + inertial approach for aircraft position, with submeter accuracy in good flight conditions.

### What is in the box?

ANAFI USA should arrive as a kit, in a customised hard case. It contains:
- 1 x ANAFI USA aircraft with lens cover
- 3 x ANAFI USA batteries
- 1 x Parrot flight controller 3
- 1 x USB 4-port charger with EU plug
- 4 x USB to USB-c cables
- 1 x tablet adapter
- 1 x spare propellor set
- 2 x 64GB microSD cards, one preinstalled in the aircraft
- 1 x microSD to SD card adapter

It should also be accompanied by:
- Ulefone Armor 9 android phone with Freeflight 6 installed
- a laminated operations checklist
- a waterproof notebook for logging flight information

The Google account associated with the control device, and flight logs held by Parrot, is `oceanseaicenpi@gmail.com`

### Key differences from DJI aircraft

While the ANAFI USA shares many similarities with other commercial RPAS and is quite interchangeable from a piloting perspective, there are a few very important key differences:

- None of the ANAFI series of RPAS use collision avoidance. It is up to you to pilot it safely.
- Parrot RPAS do not have an ATTI mode. GNSS / inertial assistance is always on to some degree, it will warn you if flight conditions are degraded due to magnetic disturbance, and control authority will automatically become more manual.
- ANAFI motors feel 'notchy' from brand new, unlike DJI Mavic or Phantom motors which spin smoothly when tested by hand rotation. Feel carefully for 'grittiness' when testing motors instead of expecting smooth, resistance free rotation

## Operational considerations

### Pre-deployment reading

- ANAFI USA operators must read and become familiar with the ANAFI USA user manual. This resource covers all aspects of how to operate the aircraft.

### Licensing and internal training

- ANAFI USA pilots *must* hold an open A1/A3 license as a minimum requirement.
- Pilots *must* be registered with the Norwegian Polar Institute organisation for insurance purposes. Contact Marius Bratrein.
- Pilots *must* have experience piloting the ANAFI USA in a controlled environment prior to field deployment.
- For sea ice mapping flights, pilots *must* attend additional training for sea ice operations

### Permits

ANAFI USA is an EU class A aircraft, carrying visible and infrared sensors, weighing 498 grams.

- For flights in Norway and on Svalbard, observe no fly zones and sensor restrictions
- For flights off Kronprins Haakon or other vessels, contact voyage leaders and ensure that required paperwork is submitted before sailing.
- Make yoursef aware of local flight regulations for any other jurisdictions you plan to use the ANAFI USA in (eg Greenland, Canadian Arctic, Finnland, ??)

### Routine operations

- Pilots *must* observe the checklist contained in this document package
- Discuss drone operations with everybody involved. For example, on Kronprins Haakon, use pre-voyage meetings to talk about your operational plans
- *Ensure that you have permission to take photos and video of people*


### Key considerations for high latitude flight

The ANAFI USA has been flown successfully from a ship and sea ice up to 87.5 degrees north. The following points were gained from the experience, and helped a lot when operating in environments where the magnetic, inertial and GNSS components of the aircraft systems are all potentially disagreeing with each other:

- calibrate the aircraft before takeoff, every takeoff. This is a simple and fast procedure.
- hand launch from ship decks. When flying from a ship, calibrate the aircraft and then hand launch without putting the aircraft down on the metal deck
- fly with authority. Hovering is a vulnerable state for RPAS in magentically-weird environments. Try to maintain dynamic flight at all times
- land with authority. Landing is a vulnerable state due to wind shear close to the landing surface and / or magnetic disturbance on a ship deck. Line up for landing, and bring the aircraft to ground as quickly as is safe.
- double check flight modes before operations. ANAFI USA has several preset modes controlling available power and flight dynamics. Ensure that your flight mode choice gives you enough to fly in wind or other conditions - we found that flying in 'racing', or full power mode, worked best on sea ice.
