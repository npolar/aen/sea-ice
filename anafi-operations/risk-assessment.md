# Risk assessment

### ANAFI USA operations

Risk assessment for ANAFI USA operations must take place inside the formal NPI framework. For operations off IMR ships, additional risk assessments are needed for permission to operate from ships.

This document outlines *typical additions* to NPI and IMR risk assessments, when ANAFI USA operations are planned.

### Risk assessment template additions

In the template below, column headers mean:

*hazard:* an event which can cause harm to people, animals, the environment, or organisations.

*C, P, R:* Consequence, Probability, Risk (C x P). Numbers are used following this approach: [ref]

*Preventative:* measures you can take to prevent exposure to the hazard (lowering P)

*Corrective:* measures you can take to reduce the impacts of hazard exposure (lowering C)

| hazard | C | P | R | preventative | corrective | C | P | R | assessment |
|--------|---|---|---|--------------|------------|---|---|---|------------|
| pilot cold injury <br />(human)| 4 | 3 | 12 | 1. Pilot must be issued with, experienced in use of, and use appropriate clothing systems for work environment<br />2. Pilot must use a buddy system for extremity checks<br />3. Encourage a culture of not rushing, avoid 'just one last flight' syndrome. | Use corrective actions for cold injury as detailed in a general risk assessment | 1 | 3 | 3 | |
| pilot cold injury <br />(reputation)| 4 | 3 | 12 | | | 1 | 3 | 3 | |
| drone crash injuring a person<br />(human) | 3 | 2 | 6 | 1. Pilot must be licensed and trained<br />2. follow operations manual which specifies routines for minimising loss of control | 1. All personnel shall have current first aid training<br />2. Assist with injury and call for assistance (113)<br />3. Respect the injured person, provide details as needed especially if injured party is not involved in the operation | 3 | 1 | 3 | |
| drone crash damaging drone <br />(economic)| 4 | 3 | 12 | 1. Pilot must be licensed and trained<br />2. follow operations manual which specifies routines for minimising loss of control and clearance from other equipment<br />3. Ensure routine maintenance is followed (see operations manual) | 1. Ensure common drone spare parts are available<br />2. Ensure pilots have basic knowledge and training for common drone repair operations | 3 | 1 | 3 | |
| drone crash damaging other equipment <br />(economic)| 4 | 3 | 12 | 1. Pilot must be licensed and trained<br />2. follow operations manual which specifies routines for minimising loss of control and clearance from other equipment<br />3. Ensure routine maintenance is followed (see operations manual) | No corrective action possible - risk reduction is based on preventative measures | 2 | 3 | 6 | Operations should be assessed on a case by case basis |
| loss of drone <br />(economic)| 4 | 3 | 12 | 1. Pilot must be licensed and trained<br />2. follow operations manual which specifies routines for minimising loss of control and clearance from other equipment<br />3. Ensure routine maintenance is followed (see operations manual) | Make economic reparations as quickly as possible < /br>2. Understand cause of drone loss, integrate precautions into future training and operations | 2 | 3 | 6 | |
| loss of drone <br />(environment)| 4 | 3 | 12 | | 1. Recover crashed drone and all parts / chemicals / components <br />2. Make reparations for wildlife and habitat damage as needed | 2 | 3 | 6 |  |
| loss of drone <br />(reputation)| 4 | 3 | 12 | | 1. Accept responsibility, avoid defensive approaches and communication styles <br  />2. Move quickly to make any reparations needed.  | 2 | 3 | 6 |  |

### Guide to numbers

This is a quasi-quantitative risk assessment format in that we're trying to be a little bit objective about how risk is computed based on the hazard we're aiming to work with, and provide a way for others to follow our reasoning.

In the risk assessment table, the consequence and probability numbers are interpreted as follows. Note that no formal number exists for 'we encounter this hazard every time' in this table.

**Frequency**

| level | description |
|---|---|
|1 | Once in 50 years or less than once in 50 years |
|2 | Once in ten years up to once in 50 years |
|3 | Once in ten years up to once a year |
|4 | Once a month up to once a year |
|5 | Weekly |

**Consequence**

| level | human | environment | economic | reputation |
|--- | --- | --- | --- | --- |
| 1 | injury requiring first aid | insignificant damage or short recovery time  | operational or activity stop < 1 day | no or very small impact on reputation |
| 2 | injury requiring medical treatment | minor damage with short recovery time | operational or activity stop < 1 week | minimal impact on reputation, little effort to recover  |
| 3 | serious injury | minor damage with long recovery time  | operational or activity stop < 1 month | reputation damaged. Some effort (media, other) to recover |
| 4 | very serious injury / disability | long term damage with long recovery time  | operational or activity stop more than 6 months, less than 12 months | Severe reputational damage requiring significant work, staff change, or other significant mechanism to recover |
| 5 | death | very long term / irreversible damage  | operational stop > 1 year | Reputation permanently impacted |

Adapted from: https://paladinrisk.com.au/risk-tip-3-developing-consequence-matrix/
