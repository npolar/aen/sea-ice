# An overview of ANAFI USA data

ANAFI USA produces five main types of data:
- RGB still imagery
- RGB video
- Thermal infrared still imagery
- Thermal infrared video
- Aircraft positioning and timing data

Aircraft positioning and timing data are normally encoded in imagery outputs, and can be retrieved using exif readers (eg exiftool, exiv2). Full flight logs can also be retrieved from the associated account with Parrot.

## Managing data

- how? duplicate where...
- droneDB? deploy across NP for drone imagery?
