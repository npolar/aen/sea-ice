# Correcting for ice floe drift in image centres

Because sea ice drifts, effective reconstruction of terrain from imagery requires correction of image centre coordinates for sea ice motion.

The first step is to generate a sea ice drift trajectory. If dual frequency GNSS receivers are available on the ice, see the rtklib-ppp instructions for generating a precise trajectory. If not, use whatever ice drift data are available.

## Generate a time series of sea ice positions


## Extract image centre positions

...using exiftool:
`exiftool -datetimeoriginal -subsectimeoriginal -gpslatitude -gpslongitude -gpsaltitude -abovegroundaltitude -gpsxyaccuracy -gpszaccuracy -camerayawdegree -camerapitchdegree -camerarolldegreee $1/*.$2 -c "%.8f" -n -csv > $3`

## As needed exchange GPS altitude for height above ground

Open the camera centre CSV file, delete the GPSaltitude column, rename the GPSaboveground column to GPSaltitude and save it as something like `flightname-aboveground.csv`. Next, create a copy of all the images and replace their geotags using exiftool:

`exiftool -csv=flightname-aboveground.csv ./path/to/images`

Finally remove images with `_original` appended to the file name. This is now the main working image set.

## Reproject to a cartesian system

...using `proj4` or `pyproj`

`awk -F, '{print $4,$5}' cameracentresfile.csv > latlonpairsfile.txt`

then

`cs2cs -r -f "%.3f" +proj=latlong +datum=WGS84 +to +proj=utm +zone=34 +datum=WGS84 latlonpairsfile.txt > utmfile.txt`

After this probably the easiest way to proceed is to create a copy of the camera centres CSV file, and replace the lat/lon pairs with UTM northings and eastings.

Take care to note which UTM zone is used. Translating reference GPS tracks to UTM follows the same pattern - extract lat lon pairs, reproject using `cs2cs`, and reassemble the GPS trajectory file using UTM coordinates instead of lats and lons.



## Choose a 'time 0'

The time of the first image should be set as a reference time. Its position should remain unchanged, and all subsequent images will have their positions adjusted by a vector of offsets.

## interpolate times

Because GPS fixed are obtained at 0.5 or 5 seconds, and the camera shutter fires at some subsecond, we need to interpolate the GPS timestamps and positions to 1/100th of a second frequency.

Then, for each camera stamp, we search the time vector from GPS positions and assemble a new vector of GPS positions at the closest time to camera firing time.

## Generate an offsets vector

This is straightforward - subtract the GNSS location at the reference time from all other sea ice GNSS positions, leaving behind a set of offsets in X and Y for each GNSS epoch.

## Apply offsets to image positions

Add the offsets vector at image shot time to all image positions.

## Apply modified positions to images

Extract the modified image positons to a 2 column northing and easting file using a space to separate columns. Then use `cs2cs` to project back to lat/lon:

`cs2cs +proj=utm +zone=34 +datum=WGS84 +to +proj=latlon +datum=WGS84 -f "%.6f" modifiedcamerapositions-utm.txt > modifiedcamerapositions-latlon.txt`

...then make a copy of the original above ground .csv file you made earlier. In this copy, replace the GPSlatitude and GPSlongitude columns with the new lat/lon pairs you just made. Finally apply these to images using exiftool:

`exiftool -csv=modifiedlatloncoordinates.csv /path/to/images`

##Done!

Now go test it all, make an awesome drift corrected photogrammetric map!
