## Sea ice density measurements

For Nansen Legacy work sea ice density profiles have been made from ice cores using a hydrostatic weighing method, following Pustogvar and Høyland (2015).

### Equipment list
- Density scales
- 250 ml glass cylinder
- glass funnel
- hydrometer
- clear plastic bucket for holding parafin on scales
- fish lure weight
- 1kg calibration weight
- scale instructions card
- paper towel
- fritidsparafin

### General strategy

Try to organise a block of time to process many cores. That way you can leave the scales set up in the lab ready to go, instead of setting up from scratch every time.

Plan for a maximum of about 1 meter of ice per day, taking into account many small things which can delay processing.

### A day before getting started

1. check that the freezer lab is at -18 to -20.
2. Bring all instruments and materials listed above into the freezer
3. Establish a work area, covering the bench with plastic sheeting
4. Secure sheeting with spring clamps
5. Plug in and level the scale

### Once everything is frozen

1. Pour parafin into the bucket used for the scale. Fill to about 5 cm below the rim.

### Each day before measurements

1. Outside of the freezer, prepare a notebook page as pictured. *Leave one page empty for notes*:
2. Calibrate the scale using the procedure for calibration with external weights
3. Measure the density of the parafin you will use to weigh core sections, write the density in the logbook
4. Prepare the bandsaw for cutting
5. Remove any ice chips from the density scales

### Core cutting procedure

1. Using a soft pencil, measure chunks  (max 6 cm length) and mark cutting locations. Start at the top of the core being 0 cm.
2. Write minimum and maximum chunk depths in the notebook
3. Cut each section and reassemble the core in order. Alternately, arrange core chunks in buckets, *being extremely careful to avoid mixing chunk order*

### Basic sample weighing procedure

1. With the scale covered by the plastic tub, press `O/T` to zero the balance. When the little arrow shows up on the left side of the display and the display reads 0 it is ready to weigh
3. Place the ice chunk on the top tray of the density scale
4. When the little arrow on the left is displayed, record weight in the `wAir` column for the relevant chunk
5. Remove the scale cover
6. Remove the ice chunk and keep it in hand
7. Lift the sample tray so that the perforated tray is accessible
8. Place the ice on the perforated tray, and lower into the parafin
9. *If the sample is dropped after weighing in air, while being transferred to parafin, it must be re-weighed in air!*
10. check that the sample and tray are not touching the bucket sides
11. check that the sample is completely immersed. Add parafin if not, there is no need to re-weigh anything
12. Cover the scale. Once the arrow shows on the left of the display, record the weight in the `wParafin` column
13. Remove the cover, carefully remove the sample from Parafin and dry it as much as possible.
14. Place the sample in a labelled bucket, record the bucket label in the `bucket` column.

#### If many air bubbles are coming from the sample

- either record the first weight you see as soon as the arrow appears on the display,
- *Or* wait 10-15 minutes (in a warm place) and record the weight after air has escaped.

You need to decide what is important here - whether you want parafin to fill cavities or not.

#### If the sample floats

Sometimes ice is less dense than parafin. In this case we use a dense object to weigh the sample down.

1. Carefully balance a weight on top of the ice chunk and record the combined weight in parafin.
2. Record both air and parafin weights of the weighting object.


### Density equations

$\rho Parafin$ is the recorded density of parafin

$wAir$ is the sample weight in wAir

$wParafin$ is the sample weight in parafin

#### 1. Standard case

When the sample is more dense than parafin, density is computed as:

$$
\rho = \frac{wAir}{wAir - wParafin} \rho Parafin
$$


#### 2. Ice floats in parafin

If the sample is less dense than parafin we use a composite density equation. In equations here, *fish* refers to the object used to weigh down ice chunks in parafin, because we adopted a fishing lure - which looks like a fish.

$$
\rho combined = (\rho ice * Pice) + (\rho fish * Pfish)
$$

...where $P$ is the *relative volume* of each part. This is refactored to:

$$
\rho ice = \frac{(\rho combined - (\rho fish * Pfish))}{Pice}
$$

##### Getting component parts

First the density of the weight is computed:

$$\rho fish = \frac{wAir}{wAir - wParafin} \rho Parafin $$

...and then the density of combined ice + weight:

$$\rho composite = \frac{wAir(fish) + wAir(ice)}{(wAir(fish) + wAir(ice)) - wParafin(fish + ice)} \rho Parafin $$



The volume of the weight is then computed in air:

$$ Vfish = \frac{Wfish}{\rho fish} $$

...as is the composite volume (sea ice plus weight). In this equation $wComposite$ is not directly measured, it is the sum of $wAir(fish)$ and $wAir(ice)$:

$$ Vcomposite = \frac{Wcomposite}{\rho composite} $$

...and the relative volume of the fish is computed as:

$$ Pfish = \frac{Vfish}{Vcomposite} $$

The relative volume of sea ice is then:

$$ Pice = 1 - Pfish $$



### References

Pustogvar, A., Høyland, K.V., 2015. Density measurements of saline ice by hydrostatic weighing in paraffin. Proceedings of the 23rd International Conference on Port and Ocean Engineering under Arctic Conditions (POAC'15). Trondheim, Norway, June 14–18, 2015.
